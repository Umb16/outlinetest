﻿Shader "Outline3"
{
	Properties
	{
		[NoScaleOffset]
		_MainTex("Texture", 2D) = "white" {}
		_MainTex2("Texture2", 2D) = "white" {}
		_MainTex3("Texture3", 2D) = "white" {}
		_Color("Test Color", Color) = (1,1,1,1)
		_OutlineDist("Outline Sample Distance", Range(0, 5)) = 2.0
	}
		SubShader
		{
			Tags
			{
				"PreviewType" = "Plane"
				"Queue" = "Transparent"
				"RenderType" = "Transparent"
			}

			Pass
			{
				Cull Off
				ZWrite Off
				Blend SrcAlpha OneMinusSrcAlpha

				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float4 vertex : SV_POSITION;
					float2 uv : TEXCOORD0;
				};

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = v.uv;
					return o;
				}

				sampler2D _MainTex;
				float2 _MainTex_TexelSize;
				sampler2D _MainTex2;
				sampler2D _MainTex3;
				float4 _Color;
				float _OutlineDist;

				float Outline(float2 uv, float size)
				{
					float a;
					a = tex2D(_MainTex, uv + _MainTex_TexelSize * size * float2(0.923880, 0.382683)).a;
					a += tex2D(_MainTex, uv + _MainTex_TexelSize * size * float2(0.382683, 0.923880)).a;
					a += tex2D(_MainTex, uv + _MainTex_TexelSize * size * float2(-0.382683, 0.923880)).a;
					a += tex2D(_MainTex, uv + _MainTex_TexelSize * size * float2(-0.923880, 0.382683)).a;
					a += tex2D(_MainTex, uv + _MainTex_TexelSize * size * float2(-0.923880, -0.382683)).a;
					a += tex2D(_MainTex, uv + _MainTex_TexelSize * size * float2(-0.382683, -0.923880)).a;
					a += tex2D(_MainTex, uv + _MainTex_TexelSize * size * float2(0.382683, -0.923880)).a;
					a += tex2D(_MainTex, uv + _MainTex_TexelSize * size * float2(0.923880, -0.382683)).a;
					return a;
				}

				fixed4 frag(v2f i) : SV_Target
				{
					fixed4 col = tex2D(_MainTex, i.uv);
					fixed4 col2 = tex2D(_MainTex2, i.uv);
					fixed4 col3 = tex2D(_MainTex3, i.uv);
					float alpha = 0;

					if (col3.r < 0.1 && col.a < 0.1)
					{
						alpha = min(1, (Outline(i.uv, _OutlineDist)));
					}
					if (alpha > 0.1)
						return col2 * (1 - _Color.a) + _Color * _Color.a;

					else
						return col2;
				}
				ENDCG
			}
		}
}
