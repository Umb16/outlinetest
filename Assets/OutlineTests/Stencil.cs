﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stencil : MonoBehaviour {

   // public RenderTexture renderTextureTarget;
    public Material matStencilToMask;
    // Use this for initialization
    void Start () {
        //renderTextureTarget = new RenderTexture(Screen.width, Screen.height, 24, RenderTextureFormat.Default);
        //GetComponent<Camera>().targetTexture = renderTextureTarget;
    }
    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {

        RenderTexture buffer = RenderTexture.GetTemporary(source.width, source.height, 24);
        //Graphics.SetRenderTarget(buffer);
       // GL.Clear(false, true, new Color(0, 0, 0, 0));   // clear the full RT

        //To keep the stencil in post process
        Graphics.SetRenderTarget(buffer.colorBuffer, source.depthBuffer);
        Graphics.Blit(buffer, matStencilToMask);
        Graphics.Blit(buffer, destination);
        buffer.Release();
    }
    // Update is called once per frame
    void Update () {
		
	}
}
