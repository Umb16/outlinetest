﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineTest : MonoBehaviour
{
    public Material mat;
    public Material mat2;
    public Material mat3;
    public Material mat4;
    public Material mat5;
    public Material matStencilToMask;
    public Camera camera;
    [Range(0.1f, 4)]
    public float resolution;
    float oldResolution;
    bool on;
    int mode = 4;
    public RenderTexture outlineCameraLow;
    public RenderTexture outlineCamera;
    public RenderTexture lowTemp;
    public RenderTexture outlineResolutionTemp;
    float lineWidth = 5;
    public GameObject cubs;
    public GameObject cube;
    // Start is called before the first frame update
    void Start()
    {
        ResolutionRefresh();
    }

    void OnGUI()
    {
        if (GUI.Button(new Rect(0, 30, 200, 170), "On " + on))
        {
            on = !on;
        }
        if (GUI.Button(new Rect(200, 0, 200, 200), "resolution " + resolution))
        {
            resolution *= .75f;
            if (resolution < .2f)
                resolution = 3;
        }
        if (GUI.Button(new Rect(400, 0, 200, 200), "mode " + mode))
        {
            mode++;
            if (mode > 2)
                mode = 0;
        }
        if (GUI.Button(new Rect(0, 200, 200, 200), "Line Width " + lineWidth))
        {
            lineWidth++;
            if (lineWidth > 5)
                lineWidth = 1;
            mat.SetFloat("_OutlineDist", resolution * lineWidth);
            mat3.SetFloat("_OutlineDist", resolution * lineWidth);
            mat4.SetFloat("_OutlineDist", resolution * lineWidth* resolution);
        }
        if (GUI.Button(new Rect(200, 200, 200, 200), "Cubecount"))
        {
            if (cubs.activeSelf)
            {
                cubs.SetActive(false);
                cube.SetActive(true);
            }
            else
            {
                cubs.SetActive(true);
                cube.SetActive(false);
            }
        }
    }

    void ResolutionRefresh()
    {
        if (camera.targetTexture != null)
        {
            camera.targetTexture.Release();
        }
        mat.SetFloat("_OutlineDist", resolution * lineWidth);
        mat3.SetFloat("_OutlineDist", resolution * lineWidth);
        mat4.SetFloat("_OutlineDist", resolution * lineWidth* resolution);
        outlineCameraLow = new RenderTexture((int)(Screen.width * 0.1f), (int)(Screen.height * 0.1f), 0);
        lowTemp = new RenderTexture((int)(Screen.width * 0.1f), (int)(Screen.height * 0.1f), 0);
        outlineCamera = new RenderTexture((int)(Screen.width * resolution), (int)(Screen.height * resolution), 0);
        outlineResolutionTemp = new RenderTexture((int)(Screen.width * resolution), (int)(Screen.height * resolution), 0);
        Resources.UnloadUnusedAssets();
    }

    // Update is called once per frame
    void Update()
    {
        if (oldResolution != resolution)
        {
            ResolutionRefresh();
            oldResolution = resolution;
        }

    }

    void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        if (on)
        {
            if (mode == 2)
            {
                camera.targetTexture = outlineCamera;
                camera.Render();
                camera.targetTexture = outlineCameraLow;
                camera.Render();

                Graphics.Blit(outlineCameraLow, lowTemp, mat2);
                mat4.SetTexture("_MainTex2", lowTemp);
                Graphics.Blit(outlineCamera, outlineResolutionTemp, mat4);
                mat5.SetTexture("_MainTex2", outlineResolutionTemp);
                Graphics.Blit(src, dest, mat5);
            }
            if (mode == 1)
            {
                camera.targetTexture = outlineCamera;
                camera.Render();
                camera.targetTexture = outlineCameraLow;
                camera.Render();
                Graphics.Blit(outlineCameraLow, lowTemp, mat2);
                //Graphics.Blit(normal2, dest);
                mat3.SetTexture("_MainTex2", src);
                mat3.SetTexture("_MainTex3", lowTemp);
                Graphics.Blit(outlineCamera, dest, mat3);
            }
            if (mode == 0)
            {
                camera.targetTexture = outlineCamera;
                camera.Render();
                mat.SetTexture("_MainTex2", src);
                Graphics.Blit(outlineCamera, dest, mat);
            }
            if (mode == 4)
            {
                //camera.targetTexture = outlineCamera;
                //camera.Render();
                RenderTexture buffer = RenderTexture.GetTemporary(src.width, src.height, 24);
                Graphics.SetRenderTarget(buffer);
                GL.Clear(false, true, new Color(0, 0, 0, 0));
                Graphics.SetRenderTarget(buffer.colorBuffer, src.depthBuffer);
                Graphics.Blit(buffer, matStencilToMask);
                mat.SetTexture("_MainTex2", src);
                Graphics.Blit(buffer, dest, mat);
                buffer.Release();
            }

        }
        else
        {
            Graphics.Blit(src, dest);
        }
    }
}
