﻿Shader "Outline2"
{
	Properties
	{
		[NoScaleOffset]
		_MainTex("Texture", 2D) = "white" {}
		_OutlineDist("Outline Sample Distance", Range(0, 5)) = 2.0
	}
		SubShader
		{
			Tags
			{
				"PreviewType" = "Plane"
				"Queue" = "Transparent"
				"RenderType" = "Transparent"
			}

			Pass
			{
				Cull Off
				ZWrite Off
				Blend SrcAlpha OneMinusSrcAlpha

				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float4 vertex : SV_POSITION;
					float2 uv : TEXCOORD0;
				};

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = v.uv;
					return o;
				}

				sampler2D _MainTex;
				float2 _MainTex_TexelSize;
				float4 _Color;
				float _OutlineDist;

				float Outline(float2 uv, float size)
				{
					float a;
					a = tex2D(_MainTex, uv + _MainTex_TexelSize * size * float2(0.923880, 0.382683)).a;
					a += tex2D(_MainTex, uv + _MainTex_TexelSize * size * float2(0.382683, 0.923880)).a;
					a += tex2D(_MainTex, uv + _MainTex_TexelSize * size * float2(-0.382683, 0.923880)).a;
					a += tex2D(_MainTex, uv + _MainTex_TexelSize * size * float2(-0.923880, 0.382683)).a;
					a += tex2D(_MainTex, uv + _MainTex_TexelSize * size * float2(-0.923880, -0.382683)).a;
					a += tex2D(_MainTex, uv + _MainTex_TexelSize * size * float2(-0.382683, -0.923880)).a;
					a += tex2D(_MainTex, uv + _MainTex_TexelSize * size * float2(0.382683, -0.923880)).a;
					a += tex2D(_MainTex, uv + _MainTex_TexelSize * size * float2(0.923880, -0.382683)).a;
					a += tex2D(_MainTex, uv + _MainTex_TexelSize * size * float2(0.923880, -0.382683)).a;
					//a += tex2D(_MainTex, uv).a;
					return a;
				}

				fixed4 frag(v2f i) : SV_Target
				{
					fixed4 col = tex2D(_MainTex, i.uv);
					float alpha = 0;
					float alpha2 = 0;
						alpha = min(1, (Outline(i.uv, _OutlineDist)) * 10);
						if (alpha > 0)
							return fixed4(0, 0, 0, 1);
						else
							return fixed4(1,1,1,1);
					}
					ENDCG
				}
		}
}
