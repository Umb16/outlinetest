using UnityEngine;
using System.Collections;


public class GUI_FPS : MonoBehaviour
{
    float timer;
    int frameCount = 0;
    int fps = 0;
    public static float screenK = 1;
    private bool _show = true;
    void Start()
    {
    }

    void OnGUI()
    {
        if (_show)
        {
            GUILayout.Box(string.Format(" fps:{0}", fps));

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_show)
        {
            frameCount++;
            if (timer <= Time.realtimeSinceStartup)
            {
                timer = Time.realtimeSinceStartup + 1;
                fps = frameCount;
                //fpsText.text=fps.ToString();
                frameCount = 0;
            }
        }
        if (Input.GetKeyUp(KeyCode.F2))
            _show = !_show;
    }
}